﻿using UnityEngine;
using System.Collections;



//este ejercicio consiste en que al pasar por una plataforma se active un sonido
//primer intento version 0.1


public class ejercicio1 : MonoBehaviour {

	public AudioClip sonido1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider parametro) {
		if (parametro.gameObject.tag == "plataforma") 
		{
			AudioSource.PlayClipAtPoint (sonido1, transform.position);
	
		}

	}
}
