﻿using UnityEngine;
using System.Collections;

//ejercicio 4 sonido e item cambio a transparente

public class ejercicio4 : MonoBehaviour {


	public AudioClip sonido1;
	public AudioClip item;
	public GameObject[] puerta;
	public int aleatorio;
	public Material transparente;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider parametro) {
		if (parametro.gameObject.tag == "plataforma") 
		{
			for (int i = 0;i<puerta.Length;i++) {
				puerta[i].GetComponet<Collider>().isTrigger = false;
			}
			aleatorio = Random.Range(0,puerta.Length);
			puerta[aleatorio].GetComponent<Collider>().isTrigger = true;
			AudioSource.PlayClipAtPoint (sonido1, transform.position);

		}

		if (parametro.gameObject.tag == "item") 
		
			{
				parametro.gameObject.GetComponent<Renderer> ().material = transparente;
				AudioSource.PlayClipAtPoint (item, transform.position);
			}
	
	}
}