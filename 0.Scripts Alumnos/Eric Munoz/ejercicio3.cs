﻿using UnityEngine;
using System.Collections;


//ejercicio 3 activa puerta de forma aleatorio

public class ejercicio3 : MonoBehaviour {

	public AudioClip sonido1;
	public GameObject[] puerta;
	public int aleatorio;



	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider parametro) {
		if (parametro.gameObject.tag == "plataforma") 
		{
			for (int i = 0;i<puerta.Length;i++) {
				puerta[i].GetComponet<Collider>().isTrigger = false;
			}
			aleatorio = Random.Range(0,puerta.Length);
			puerta[aleatorio].GetComponent<Collider>().isTrigger = true;
			AudioSource.PlayClipAtPoint (sonido1, transform.position);

		}

	}
}