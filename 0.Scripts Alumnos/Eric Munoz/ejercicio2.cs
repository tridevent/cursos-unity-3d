﻿using UnityEngine;
using System.Collections;

// ejercicio 2 con c#

public class ejercicio2 : MonoBehaviour {

	public AudioClip sonido1;
	public GameObject puerta;



	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider parametro) {
		if (parametro.gameObject.tag == "plataforma") 
		{

			puerta.GetComponent<Collider> ().isTrigger = true;
			AudioSource.PlayClipAtPoint (sonido1, transform.position);

		}

	}
}
