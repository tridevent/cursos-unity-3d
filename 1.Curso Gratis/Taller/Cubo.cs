﻿using UnityEngine;
using System.Collections;

public class Cubo : MonoBehaviour {
    public Vector3 velocidad;

	void Start () {
	
	}

	void Update () {
        if(Input.GetMouseButton(0))
        {
            this.gameObject.transform.Translate(velocidad);
        }
        if (Input.GetMouseButton(1))
        {
            this.gameObject.transform.Rotate(velocidad*10);
        }
        if (Input.GetMouseButton(2))
        {
            this.gameObject.transform.localScale += velocidad;
        }
        if(Input.GetKeyUp(KeyCode.E))
        {
            velocidad = velocidad * (-1);
        }
	}
}
