﻿using UnityEngine;
using System.Collections;

public class Personaje : MonoBehaviour {
    public int velocidad;
    public float fuerza;
    public bool suelo;
    private Rigidbody rb;

	void Start () {
        rb= gameObject.GetComponent<Rigidbody>();
	}
	
	void Update () {
        Vector3 ejex = Input.GetAxis("Horizontal") * transform.right * Time.deltaTime * velocidad; 
        Vector3 ejez = Input.GetAxis("Vertical") * transform.forward * Time.deltaTime * velocidad;
        transform.Translate(ejex + ejez);

        //if(Input.GetKeyDown(KeyCode.Space))
        if (Input.GetButtonDown("Jump"))
        {
            Salto();
        }
    }
    void Salto()
    {
        if (suelo == true)
        {
            rb.AddForce(Vector3.up * fuerza);
            suelo = false;
        }
    }
    void OnCollisionEnter(Collision colision)
    {
        suelo = true;
    }
}
