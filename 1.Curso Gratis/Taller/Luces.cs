﻿using UnityEngine;
using System.Collections;

public class Luces : MonoBehaviour {
    public GameObject luz;
    public bool cambioColor;
    private Light componenteLuz;

	void Start () {
       componenteLuz=  luz.GetComponent<Light>();
        cambioColor = false;
	}
	
	void Update () {
	if(Input.GetKeyUp(KeyCode.P))
        {
            //Prender
            componenteLuz.enabled = !componenteLuz.enabled;
        }
    if(Input.GetKeyUp(KeyCode.C))
        {
            //Cambiar color
            cambioColor = !cambioColor;
            if(cambioColor)
            {
                componenteLuz.color = Color.green; 
            }
            else
            {
                componenteLuz.color = Color.blue;
            }
        }
    if(Input.GetKeyUp(KeyCode.M))
        {
            componenteLuz.intensity += 0.5f;
        }
    if (Input.GetKeyUp(KeyCode.N))
        {
            componenteLuz.intensity -= 0.5f;
        }
    }
}
